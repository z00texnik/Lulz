package ru.pizzaHut;

import org.junit.*;
import org.openqa.selenium.*;
import org.openqa.selenium.WebDriver;
import com.codeborne.selenide.Configuration;
import static com.codeborne.selenide.Selenide.*;
import static com.codeborne.selenide.Condition.*;
import static com.codeborne.selenide.WebDriverRunner.CHROME;
import static com.codeborne.selenide.WebDriverRunner.closeWebDriver;

public class AuthTrue {
    private static WebDriver driver;

    @Before
    public void setUp() throws Exception {
        Configuration.browser = CHROME;
        Configuration.browserSize = "1920x1080";
        Configuration.baseUrl = "http://pizzahut.ru";
    }

    @Test
    public void FirstUno() throws Exception {
        open("");
        //Вводим логин
        $(By.cssSelector(".login-table .login"))
                .shouldBe(visible)
                .setValue("9994519098");
        //Вводим пароль от логина
        $(By.cssSelector(".login-table .password"))
                .shouldBe(visible)
                .setValue("Qwerty1234");
        //Жмакаем на кнопку войти
        $(By.cssSelector(".login-table button"))
                .shouldBe(enabled)
                .click();
        //Ищем нужное имя профиля
        $(By.cssSelector(".resp-hidden-sm .account-name"))
                .waitUntil(visible, 10000)
                .shouldHave(text("Дмитрий"));
        $(By.cssSelector(".resp-hidden-sm .logout"))
                .shouldBe(visible)
                .click();

        }
        @AfterClass
    public static void endBrowser() {closeWebDriver();}

}
