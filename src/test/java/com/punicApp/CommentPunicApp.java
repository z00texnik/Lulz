package com.punicApp;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.Configuration;
import org.junit.*;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.*;
import static com.codeborne.selenide.WebDriverRunner.CHROME;
import static com.codeborne.selenide.Selenide.open;
import static com.codeborne.selenide.WebDriverRunner.closeWebDriver;

public class CommentPunicApp {

    @Before
    public void setUp() throws Exception {
        Configuration.browser = CHROME;
        Configuration.browserSize = "1920x1080";
        Configuration.baseUrl = "https://stage.punicapp.com/";

    }

    @Test
    public void Invalid() throws Exception {
        open("");
        $(By.cssSelector("form .pa-input__wrapper:nth-of-type(1) [type]"))
                .shouldBe(Condition.visible)
                .setValue("I testing");
        $(By.cssSelector("form .pa-input__wrapper:nth-of-type(2) [type]"))
                .shouldBe(Condition.visible)
                .setValue("For lulz");
        $(By.cssSelector("textarea"))
                .shouldBe(Condition.visible)
                .setValue("Пишем комментарий для простоты всякого такого. Простите, что занимаюсь такой фигнёй, но я постигаю автотесты, учусь и всё такое");
        $(By.cssSelector(".pa-button--block"))
                .shouldBe(Condition.enabled)
                .waitUntil(Condition.visible, 5000)
                .click();
        $(By.cssSelector(".alert-success"))
                .waitUntil(Condition.visible, 5000)
                .shouldHave(Condition.text("Спасибо за ваше обращение!"));

    }
    @AfterClass
    public static void endBrowser() {closeWebDriver();}
}
