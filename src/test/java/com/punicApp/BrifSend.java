package com.punicApp;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.junit.TextReport;
import org.junit.*;
import com.codeborne.selenide.Configuration;
import org.junit.rules.TestRule;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.open;
import static com.codeborne.selenide.WebDriverRunner.CHROME;

public class BrifSend {
    @Rule
    public TestRule report = new TextReport().onFailedTest(true).onSucceededTest(true);
    @Before
    public void setUp() throws Exception {
        Configuration.browser = CHROME;
        Configuration.browserSize = "1920x1080";
        Configuration.baseUrl = "https://stage.punicapp.com/";

    }
    @Test
    public void Brif() throws Exception {
        open("https://stage.punicapp.com/");
        $(By.linkText("Заполните бриф"))
                .shouldBe(Condition.visible)
                .click();

    }
}