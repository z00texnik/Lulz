package com.punicApp;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.junit.TextReport;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestRule;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.open;
import static com.codeborne.selenide.WebDriverRunner.CHROME;

public class Consultate {
    @Rule
    public TestRule report = new TextReport()
            .onFailedTest(true)
            .onSucceededTest(true);

    @Before
    public void setUp() {
        Configuration.browser = CHROME;
        Configuration.browserSize = "1920x1080";
        Configuration.baseUrl = "http://stage.punicapp.com";
    }
    @Test
    public void Consultate() {
        open ("");
        $(By.cssSelector("[class=\"col-12 col-lg-5 order-md-3 py-2\"]"))
                .shouldBe(Condition.enabled)
                .click();
        $(By.cssSelector(".form-controls .pa-input__wrapper:nth-of-type(1) [type]"))
                .shouldBe(Condition.visible)
                .val("Свяжитесь со мной ниткой");
        $(By.cssSelector(".form-controls .pa-input__wrapper--no-border [type]"))
                .shouldBe((Condition.visible))
                .val("Обрайщайтесь со мной аккуратно");
        $(By.cssSelector(".pa-button--primary.pa-button--block"))
                .shouldBe(Condition.enabled)
                .click();

    }
}
