package com.punicApp;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.junit.TextReport;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestRule;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.open;
import static com.codeborne.selenide.WebDriverRunner.CHROME;
import static com.codeborne.selenide.Selenide.*;

public class Portfolio {
    @Rule
    public TestRule report = new TextReport()
            .onFailedTest(true)
            .onSucceededTest(true);
    @Before
    public void setUp() {
        Configuration.browser = CHROME;
        Configuration.browserSize = "1920x1080";
        Configuration.baseUrl = "https://stage.punicapp.com";
    }
    @Test
    public void Portfolio() {
        open("");
        $(By.cssSelector(".navbar__menu-wrapper--navbar > .navbar-menu:nth-of-type(1) > .navbar-menu__item:nth-of-type(3) .navbar-menu__item__wrapper"))
                .shouldBe(Condition.visible)
                .shouldBe(Condition.enabled)
                .click();
        $(By.cssSelector(".portfolio .portfolio-item--left:nth-of-type(1) .responsive-small"))
                .shouldBe(Condition.visible);
        $(By.cssSelector(".portfolio .portfolio-item--left:nth-of-type(1) .portfolio-item__learn-more"))
                .shouldBe(Condition.enabled)
                .click();
    }
}
